﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class MazeGenerator : MonoBehaviour
{
    public bool randomDivisions;
    const int basePlaneSize = 5;
	void Awake ()
	{
	    int mazeWidth  = (int) transform.localScale.x * 2 * basePlaneSize;
	    int mazeHeight = (int) transform.localScale.z * 2 * basePlaneSize;

	    MazeRoom.RandomDivisions = randomDivisions;
	    MazeRoom maze = new MazeRoom(0,0, mazeWidth, mazeHeight);
	    Vector3 mazeOrigin = transform.position;
	    mazeOrigin.x -= transform.localScale.x * basePlaneSize;
	    mazeOrigin.z -= transform.localScale.z * basePlaneSize;
	    mazeOrigin.y += 0.5f;
        maze.BuildWalls(mazeOrigin);

	    List<NavMeshBuildMarkup> walkable = new List<NavMeshBuildMarkup>
	    {
	        new NavMeshBuildMarkup
	        {
	            area = NavMesh.GetAreaFromName("Walkable"),
	            overrideArea = true,
	            root = transform,
                ignoreFromBuild = false
	        }
	    };
        
	    List<NavMeshBuildSource> items = new List<NavMeshBuildSource>();
        NavMeshBuilder.CollectSources(
            null,
            LayerMask.GetMask("Maze"),
            NavMeshCollectGeometry.PhysicsColliders, 
            NavMesh.GetAreaFromName("Not Walkable"),
            walkable,
            items 
            );

	    
	    NavMeshData navMeshData = NavMeshBuilder.BuildNavMeshData(
            new NavMeshBuildSettings
            {
                agentRadius = 0.25f,
                agentHeight = 1f,
            }, 
            items,
            new Bounds(Vector3.zero, new Vector3(50,50,50)),
            transform.position,
            transform.rotation 
	    );

	    NavMesh.AddNavMeshData(navMeshData);
	}
}

class MazeRoom
{
    public static bool RandomDivisions;
    private int xOrigin, yOrigin;
    private int height, width;
    private bool isLeaf;
    private int hPos, vPos;
    private MazeRoom nw, ne, se, sw;
    private int closedDoor; // 0 == N, 1 == E, 2 == S, 3 == O

    public void BuildWalls(Vector3 mazeOrigin)
    {
        if (isLeaf) return; // Sala sem divisoes
        // Vertical, topo (N) == 0
        BuildVHoleWall(closedDoor != 0, mazeOrigin,
            new Vector3(xOrigin + hPos, 0, yOrigin),
            new Vector3(xOrigin + hPos, 0, yOrigin + vPos));
        // Vertical, fundo (S) == 2
        BuildVHoleWall(closedDoor != 2, mazeOrigin,
            new Vector3(xOrigin + hPos, 0, yOrigin + vPos),
            new Vector3(xOrigin + hPos, 0, yOrigin + height));
        // Horizontal, esquerdo (O) = 3
        BuildHHoleWall(closedDoor != 3, mazeOrigin,
            new Vector3(xOrigin, 0, yOrigin + vPos),
            new Vector3(xOrigin + hPos, 0, yOrigin + vPos));
        //Horizontal, direito (E) == 1
        BuildHHoleWall(closedDoor != 1, mazeOrigin,
            new Vector3(xOrigin + hPos, 0, yOrigin + vPos),
            new Vector3(xOrigin + width, 0, yOrigin + vPos));

        nw.BuildWalls(mazeOrigin);
        ne.BuildWalls(mazeOrigin);
        se.BuildWalls(mazeOrigin);
        sw.BuildWalls(mazeOrigin);

    }

    void BuildVHoleWall(bool hasDoor, Vector3 origin, Vector3 p1, Vector3 p2)
    {
        if (hasDoor)
        {   // Criar duas paredes
            int doorPos = Random.Range(0, (int)(p2.z - p1.z));
            BuildVWall(origin, p1, p1 + Vector3.forward * doorPos);
            BuildVWall(origin, p1 + (Vector3.forward * (doorPos + 1)), p2);
            
        }
        else // Sem porta
            BuildVWall(origin, p1, p2);
    }

    void BuildHHoleWall(bool hasDoor, Vector3 origin, Vector3 p1, Vector3 p2)
    {
        if (hasDoor)
        {   // Criar duas paredes
            int doorPos = Random.Range(0, (int)(p2.x - p1.x));
            BuildHWall(origin, p1, p1 + Vector3.right * doorPos);
            BuildHWall(origin, p1 + (Vector3.right * (doorPos + 1)), p2);
        }
        else // Sem porta
            BuildHWall(origin, p1, p2);
    }
    void BuildVWall(Vector3 mazeOrigin, Vector3 p1, Vector3 p2)
    {
        if ((p2 - p1).magnitude < 0.2f) return; // NO WALL
        p1 += mazeOrigin;  p2 += mazeOrigin;
        GameObject wall = GameObject.CreatePrimitive(PrimitiveType.Cube);
        Vector3 center = new Vector3(p1.x, p1.y, (p2.z-p1.z)/2f + p1.z);
        wall.transform.position = center;
        Vector3 scale = new Vector3(0.1f, 1f, p2.z - p1.z);
        wall.transform.localScale = scale;
        wall.layer = LayerMask.NameToLayer("Maze");
    }

    void BuildHWall(Vector3 mazeOrigin, Vector3 p1, Vector3 p2)
    {
        if ((p2 - p1).magnitude < 0.2f) return; // NO WALL
        p1 += mazeOrigin; p2 += mazeOrigin;
        GameObject wall = GameObject.CreatePrimitive(PrimitiveType.Cube);
        Vector3 center = new Vector3((p2.x-p1.x)/2f + p1.x, p1.y, p1.z);
        wall.transform.position = center;
        Vector3 scale = new Vector3(p2.x - p1.x, 1f, 0.1f);
        wall.transform.localScale = scale;
        wall.layer = LayerMask.NameToLayer("Maze");
    }


    public MazeRoom(int oX, int oY, int w, int h)
    {
        xOrigin = oX; yOrigin = oY;
        width = w; height = h;
        if (w == 1 || h == 1) isLeaf = true;
        else
        {
            isLeaf = false;
            closedDoor = Random.Range(0, 3);
            hPos = MazeRoom.RandomDivisions ? Random.Range(1, w) : w / 2;
            vPos = MazeRoom.RandomDivisions ? Random.Range(1, h) : h / 2; 
            nw = new MazeRoom(oX, oY, hPos, vPos);
            ne = new MazeRoom(oX + hPos, oY, w-hPos, vPos);
            sw = new MazeRoom(oX, oY + vPos, hPos, h - vPos);
            se = new MazeRoom(oX+hPos, oY+vPos, w-hPos, h-vPos);
        }
    }
}


